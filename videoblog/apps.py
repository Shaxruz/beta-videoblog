from django.apps import AppConfig


class VideoblogConfig(AppConfig):
    name = 'videoblog'
    verbose_name = 'Блог'
